console.log('Practice Hello World');

let favoriteFoods = ["Adobo", "Pizza", "Lechon",];
console.log(favoriteFoods);

//***PUSH METHOD (ADDS AN ELEMENT AT THE END OF AN ARRAY)
favoriteFoods.push('Burger');
console.log(favoriteFoods);

//PUSH METHOD (FUNCTION) (PLACEHOLDER PARAM NEEDED) (ADDS AN ELEMENT AT THE END OF AN ARRAY)
function pushFavoriteFood(x) {
    favoriteFoods.push(x)
}
pushFavoriteFood('Sashimi');
console.log(favoriteFoods)

//***POP METHOD (NO PARAM NEEDED) (REMOVE ONLY) (REMOVES AN ELEMENT AT THE END OF AN ARRAY)
favoriteFoods.pop();
console.log(favoriteFoods)

//POP METHOD (FUNCTION) (NO PARAM NEEDED) (REMOVE ONLY) (REMOVES AN ELEMENT AT THE END OF AN ARRAY)
function popFavoriteFood() {
    favoriteFoods.pop();
}

popFavoriteFood();
console.log(favoriteFoods)

//***UNSHIFT METHOD (PLACEHOLDER PARAM NEEDED) (ADDS AN ELEMENT AT THE BEGINNING OF AN ARRAY)

favoriteFoods.unshift('FISH');
console.log(favoriteFoods);

//UNSHIFT METHOD (FUNCTION) (PLACEHOLDER PARAM NEEDED) (ADDS AN ELEMENT AT THE BEGINNING OF AN ARRAY)
function unshiftMethod(params) {
    favoriteFoods.unshift(params);
}

unshiftMethod('EGGS');
console.log(favoriteFoods)

//***SHIFT METHOD (REMOVES AN ELEMENT AT THE BEGINNING OF AN ARRAY) (PLACEHOLDER PARAM NEEDED)

favoriteFoods.shift();
console.log(favoriteFoods)

//SHIFT METHOD (FUNCTION) (REMOVES AN ELEMENT AT THE BEGINNING OF AN ARRAY) (PLACEHOLDER PARAM NEEDED)

function shiftMethod(x) {
    favoriteFoods.shift(x);
}

shiftMethod();
console.log(favoriteFoods)

//***SORT METHOD

let numbers = [5, 20, 15, 10, 25];

// ASCENDING ORDER
numbers.sort(
    function (a, b) {
        return a - b
    }
)

console.log(numbers)

//DESCENDING ORDER
numbers.sort(
    function (a, b) {
        return b - a;
    }
)

console.log(numbers);

//***REVERSE METHOD

numbers.reverse()
console.log(numbers)

//***SPLICE METHOD */
/* no param = removes all */

/* let digits = numbers.splice(2,1);
console.log(numbers);
console.log(digits); */

//***SLICE METHOD */

/* let slicedNumbers = numbers.slice(3,5)
console.log(slicedNumbers)
console.log(numbers) */

//***CONCATENATION */

console.log(favoriteFoods)
console.log(numbers)
let pets = ["dog", "cat", "rabbit"]
console.log(pets)

let myConcat = favoriteFoods.concat(numbers, pets);
console.log(myConcat)

let sports = ["basketball", "tennis", "golf"];
let myConcat2 = myConcat.concat(sports)
console.log(myConcat2)

//***JOIN METHOD */

let newJoin = sports.join()
console.log(newJoin)

//***ACCESSORS */

let cars = ["BMW", "FORD", "TOYOTA", "NISSAN", "HONDA", "MITSUBISHI", "AUDI", "BMW"];
console.log(cars)

//determines the index of the specified element/parameter
console.log(cars.indexOf("FORD"));
console.log(cars.indexOf("NISSAN"));

console.log(cars.lastIndexOf("BMW"));
//starts the search for the index from the end of the array
console.log(cars.indexOf("BMW"));

//***REASSIGNMENT OF THE ELEMENTS AND INDEX */
cars[4] = "CITY"
console.log(cars)

//***ITERATOR */
//FOR EACH

let colors = ["red", "orange", "yellow", "green", "blue", "indigo", "violet"];
console.log(colors)

colors.forEach(
    function (params) {
        console.log(params)
    }
)

//***MAP METHOD */

let mapColors = colors.map(
    function (params) {
        return `${params} is one of the colors of the rainbow`
    }
)

console.log(mapColors);
console.log(colors);

//CHALLENGE

let rainbowColors = [];
console.log(rainbowColors);

colors.forEach(
    function (x) {
        rainbowColors.push(x)
    }
)

console.log(rainbowColors);

//==========================================================

let rainbowColors2 = [];

rainbowColors.forEach(
    function (x) {
        console.log(x)
    }
)

rainbowColors.forEach(
    function (x) {
        rainbowColors2.push(x)
    }
)

console.log(rainbowColors2)