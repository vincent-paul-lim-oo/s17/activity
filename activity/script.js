let student = [];
console.log(student);

function addStudent(x) {
    student.push(x);
    console.log(x + ` ` +`was added to the student's list.`);
}

function countStudents() {
    student.length
    console.log(`There are a total of `+student.length + ` ` + `students enrolled.`)
}

function printStudents() {
    student.sort();
    student.forEach(x => console.log(x));
}

function findStudent(stud) {

	const foundStudents = student.filter( 
		function(s) {
			return stud.toLowerCase() === s.toLowerCase()
		}
	)

	const capitalName = stud.charAt(0).toUpperCase() + stud.slice(1)

	if(foundStudents.length === 0) {
		console.log(capitalName + " is not an enrollee.")
	} else if(foundStudents.length > 1) {
		console.log(capitalName + " are enrollees.")
	} else if(foundStudents.length === 1) {
		console.log(capitalName + " is an enrollee.")
	}
}
