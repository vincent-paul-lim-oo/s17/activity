/* 
Mini activity
How do we display the following tasks in the console?

drink HTML
eat JavaScript
inhale CSS
bake Bootstrap

send a screenshot of the output in the Hangouts
*/

//try
/* let number = [1 + 1, 2 - 2]
console.log(number) */

/* 
Arrays are used to store multiple related data inside a single variable
They are declared through the square brackets ([]), "Array Literals"
Arrays are constructed if there is a need to manipulate the related values inside the variable.

Number of elements in an array corresponds to the total amount of the elements
The position of each element is called index. Calling an element through its index is determined by using either the syntax:

<arrayName>[index]

Determining the index of the element:
index - is the position of the element in the array
    - start counting from 0
    - total number of elements -1
*/


/* console.log(["drink HTML", "eat JavaScript", "inhale CSS", "bake Bootstrap"]); */

//total number of elements = 4
    let tasks = ["drink HTML", "eat JavaScript", "inhale CSS", "bake Bootstrap"];
    console.log(tasks);

//to display the first element
    console.log(tasks[0])

//to display the last element
    let indexOfLastElement = tasks.length - 1;
    console.log(tasks[indexOfLastElement]);

//to display the element in the middle
    let indexOfElement = tasks.length - 2;
    console.log(tasks[indexOfElement]);
    
//--------------------------------------------------
// ARRAY MANIPULATION
// setting the array

    let numbers = ["one", "two", "three", "four"];
    console.log(numbers);

// adding element
// ***PUSH METHOD***
    //
    /*
        Syntax
        arrayName.push(element);
    */

    numbers.push("five");
    console.log(numbers);

// pushing an element inside a function

    function pushMethod(element) {
        numbers.push(element);
    }
    pushMethod("six");
    pushMethod("seven");
    pushMethod("eight");
    console.log(numbers);

//------------------------------------------------
//adding an element to be removed

    pushMethod("CrushAkoNgCrushKo");
    console.log(numbers);

//remove an element at the end of an array
// ***POP METHOD***
    //parameters do not work

    numbers.pop();
    console.log(numbers)

// function method

    function popMethod() {
        numbers.pop();
    }
    popMethod();
    console.log(numbers)

//-----------------------------------------------
//adding an element at the beginning of an array
// ***UNSHIFT METHOD***
    //adds an element at the start of an array

    numbers.unshift("zero");
    console.log(numbers);

// function method

    function unshiftMethod(element) {
        numbers.unshift(element);
    }
    unshiftMethod("jm pogi");
    unshiftMethod("hugot")
    unshiftMethod("sanaol")
    unshiftMethod("ditoNaKo")
    console.log(numbers);

//----------------------------------------------
// ***SHIFT METHOD***
    // removes an element at the beginning of an array
    // numbers.shift
    // parameters do not work

    numbers.shift();
    console.log(numbers)

// function method

    function shiftMethod() {
        numbers.shift();
    }

    shiftMethod();
    console.log(numbers);

//------------------------------------------------
    let numbs = [15, 10, 31, 24, 30]

// ***SORT METHOD***
    //arranges the order of the elements in the array.
    /* 
        return a - b
            signifies that the elements have to be rearranged in an ascending order
    */
        //ascending order
        
    numbs.sort(
        function (a,b) {
            return a - b 
        }
    )

    console.log(numbs)

        //descending order

    numbs.sort(
        function (a,b) {
            return b - a 
        }
    )

    console.log(numbs)
    
//------------------------------------------------    
// ***Reverse Method***
        // reverses the order of the elements of the last array

    numbs.reverse()
    console.log(numbs)
    
    numbs.reverse()
    console.log(numbs)

//------------------------------------------------
// ***Splice Method***
        //it directly manipulates the array that we have
        //returns the omitted elements
/* 

        first parameter determines the index on which the omission will start

        second parameter determines how many elements are to be omitted from the first parameter

        third parameter determines the replacement or the omitted element if there is a need to do so
*/
//only one part
        // will delete all of the elements on the right of the specified index
/* let nums = numbs.splice(1)
console.log(numbs)
console.log(nums) */

/* let nums = numbs.splice(3,1)
console.log(numbs)
console.log(nums)
 */
    let nums = numbs.splice(3,1,11,9,1)
    console.log(numbs)
    console.log(nums)

//------------------------------------------------    
// ***SLICE METHOD***
        //it cannot manipulate the original array.

        /* 
            first parameter - the index where the copying will start, going to the right

            second parameter - the number of elements to be copied (counting starts at the first element, not the index)
        */
    let slicedNumbs = numbs.slice(1,6)
    console.log(slicedNumbs)
    console.log(numbs)
    
//-------------------------------------------
// ***CONCATENATION***
        //merge two or more arrays
        //Concat

console.log(numbers);
console.log(numbs);
let animals = ["snake", "chimp", "dog", "cat"];
console.log(animals)

/* 
        parameters - determines the arrays to be merged with the "numbers" array
*/

let newConcat = numbers.concat(numbs, animals)
console.log(newConcat)

let animals2 = ["bird", "dinosaur"];
let concat2 = newConcat.concat(animals2);
console.log(concat2)

//------------------------------------------------
// ***JOIN METHOD***
        //merges the elements inside the array
        //no parameter - transforms the data type of the array into strings, elements are separated by comma
        //parameters - determines the means of separating elements

let meal = ["rice", "lechon kawali", "Coke"];
console.log(meal)

let newJoin = meal.join()
console.log(newJoin)

newJoin = meal.join("")
console.log(newJoin)

newJoin = meal.join(" ")
console.log(newJoin)

newJoin = meal.join("-")
console.log(newJoin)

//------------------------------------------------
// ***ACCESSORS***
        //manipulates our arrays

let countries = ["PKT", "US", "PH", "NZ", "UK", "AU", "SG", "JPN", "CA", "SK", "PH"];
// indexOf() arrayName.indexOf(element)
        //determines the index of the specified element/parameter
console.log(countries.indexOf("PH"));
// if the element is non-existing, it will return -1
console.log(countries.indexOf("RU"));
// lastIndexOf(element)
        //starts the searching for index of the element from the end of the array.
console.log(countries.lastIndexOf("PH"));

//Reassignment of the elements and index
countries[0] = "NK"
console.log(countries)

//------------------------------------------------
// ***ITERATORS***
        //manipulates our arrays
        //cb = callback function
        //forEach (cb())

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]

days.forEach(
			function(element){
				console.log(element)
			}
)

//-------------------------------------------------
// ***MAP METHOD***
            //callback function
            // (cb())
            // `` template literals

let mapDays = days.map(
    function (day) {
        return `${day} is the day of the week`
    }
)

console.log(mapDays)
console.log(days)

/* 
Mini Activity

Using forEach method, add each element of days array in an empty array called days2
*/

let days2 = [];
console.log(days2)

days.forEach(
    function (x) {
        days2.push(x)
    }
)
console.log(days2)


//***every() method***
/* 
    - it checks if all elements in an array meet the given condition
    Syntax:
        let/const resultArray = arrayName.every(
            function(indivElement) {
                return expression/condition
            }
        )
*/


let numbersA = [1, 2, 3, 4, 5]

let allValid = numbersA.every(
    function (number) {
        return (number < 3)
    }
)

console.log(allValid)

// ***some() method***
/* 
    = checks if at least one element in the array meets the given condition
    - this will return a true value if at least one element meet the conditin and false if not

    Syntax:
        let/const resultArray = arrayName.some(
            function(indivElement) {
                return expression/condition
            }
        )
*/

let someValid = numbersA.some(
    function (number) {
        return (number < 2)
    }
)

console.log(someValid)


//*** filter()method*** */
/* 
    - Returns a new array that contains elements which meets the given condition.
    - Return an empty array if no elements were found.

    Syntax:
        let/const resultArray = arrayName.filter(
            function(indivElement) {
                return expression/condition
            }
        )
*/

let filterValid = numbersA.filter(
    function (number) {
        return (number < 3)
    }
)

console.log(filterValid)

let nothingFound = numbersA.filter(
    function (number) {
        return (number = 0);
    }
)

console.log(nothingFound)

let numbersArray = [500, 12, 120, 60, 6, 8]

let divisibleBy5 = numbersArray.filter(
    function (number) {
        return (number % 5 === 0);
    }
)

console.log(divisibleBy5);

//Filtering using ForEach

let filteredNumbers = [];

numbersA.forEach(
    function (number) {
        
        if (number < 3) {
            filteredNumbers.push(number)
        }
    }
)

console.log(filteredNumbers)

//includes method

/* 
    -
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(
    function (product) {
        return product.toLowerCase().includes('a')
    }
)

console.log(filteredProducts);

//Multidimensional Arrays
/* 


*/

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
];

console.log(chessBoard);
console.log(chessBoard[1][4]); //e2
console.log("Pawn moves to: " + chessBoard[7][4]);